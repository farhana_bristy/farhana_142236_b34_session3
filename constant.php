<?php
namespace bitm;
echo __NAMESPACE__;
echo "<br>";

define("Aconstant","this is a constant");

echo Aconstant;   //for constant
echo "<br>";

echo __LINE__;   //for line number
echo "<br>";

echo __FILE__;   //for file path
echo "<br>";

function Afunction($a,$b)
{
    echo __FUNCTION__;      //to show function name
    echo "<br>";
    $c = $a + $b;
    return $c;
}
echo Afunction(23,40);    //function call & output

echo "<br>";

//echo __CLASS__;     //to show name
//echo "<br>";

echo __DIR__;   //for file directory


echo "<br>";
//echo __TRAIT__;
?>


