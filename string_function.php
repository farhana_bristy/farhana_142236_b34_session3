<?php

//Quote string with slashes
$str = "Is your name O'Really?";
echo addslashes($str);

//explode — Split a string by string
$str="I love programming";
$arr=explode(" ",$str);
echo $arr[0];

//implode —  Join array elements with a string
$str=array("I","love","programming");
$arr=implode(" ",$str);
echo $arr;
echo "<br>";

//trim — Strip whitespace (or other characters) from the beginning and end of a string
$text   = "\t\tThese are a few words :) ...  ";
var_dump($text);
$trimmed = trim($text);
var_dump($trimmed);


//nl2br — Inserts HTML line breaks before all newlines in a string
echo nl2br("Welcome\nThis is my HTML document");


echo "<br>";
//str_pad — Pad a string to a certain length with another string
$str="this is the assignment of 4th session";
echo str_pad($str,100);

echo "<br>";
//str_repeat — Repeat a string
$str1="utf";
echo str_repeat($str1,3);

echo "<br>";
//str_replace — Replace all occurrences of the search string with the replacement string
$str1="utf";
echo str_replace('u','t',$str1);

echo "<br>";
//str_split — Convert a string to an array
 $arr = "string split test";
$arr1 = str_split($arr);
print_r($arr1);
echo "<br>";
//strlen-shows the length of a string
echo strlen($arr);
echo "<br>";
//strtolower-convert string in lowercase
echo strtolower($arr);
echo "<br>";
//strtoupper-convert string in uppercase
echo strtoupper($arr);
echo "<br>";
//substr_count-counts a string from a string
echo substr_count("hiiiiii","hi");

echo "<br>";
echo substr_replace("Bangladesh"," language",6);
?>