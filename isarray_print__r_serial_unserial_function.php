<?php

//is_array — Finds whether a variable is an array
//var_dump — Dumps information about a variable

$array_value=array("rose","lily","sunflower");
$variable=0;

$array_or_not=is_array($array_value);
$array_or_not2=is_array($variable);

var_dump($array_or_not);//output 1 or 0
echo "<br>";
var_dump($array_or_not2);
echo "<br>";

//print_r — Prints human-readable information about a variable
print_r($array_value);

echo "<br>";
echo "<br>";

//serialize — Generates a storable representation of a value

$str=serialize($array_value);

//unserialize — Creates a PHP value from a stored representation

$str1=unserialize($str);


echo $str;
echo "<br>";
echo "<br>";

print_r($str1);

?>